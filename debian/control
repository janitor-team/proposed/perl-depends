Source: perl-depends
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: devel
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 12)
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/perl-depends
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/perl-depends.git
Homepage: https://github.com/jaalto/project--perl-depends

Package: perl-depends
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Description: rough indicator of Perl module dependencies
 perl-depends is a tool to show roughly what modules a program uses. Perl
 evaluates "use" commands at compile time, storing the information about
 loaded modules in the %INC variable. Comparing that list with the
 standard Perl modules gives an estimate of the external module
 dependencies.
 .
 The dependency information can be used to determine what external
 modules have to be installed before the program can be used.
